 
@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                  <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Crypto Clicks</h1>
                              @guest

                              @else
                              <h1 class="mb-2 pb-0 display-4" id="title"> User ID :{{Auth::user()->id}}</h1>
                              @endif
                                                         @if (session()->has('impersonate'))
                                     <li>
                                        <a onclick="event.preventDefault(); document.getElementById('impersonating').submit();">
                                              <i data-cs-icon="wifi" class="icon" data-cs-size="18"></i>
                                            <span class="label">Stop Impersonating</span>
                                        </a>
                                               <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                                    </li>
                                    @endif
                              <div class="text-muted font-heading text-small">Click on crypto currency to make them known. </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-12 col-lg-8 mb-5">
                           <div class="card sh-45 h-lg-100 position-relative bg-theme">
                              <img src="/img/home.jpg" class="card-img h-100 position-absolute theme-filter" alt="card image">
                              <div class="card-img-overlay d-flex flex-column justify-content-end bg-transparent">
                                 <div class="mb-4">
                                 
                                 </div>
                                 @guest
                                 <div>
                                    <a href="{{url('register')}}" class="btn btn-icon btn-icon-start btn-primary mt-3 stretched-link">
                                    <i data-cs-icon="chevron-right"></i>
                                    <span>Get Started</span>
                                    </a>
                                 </div>
                                 @else
                                   <div>
                                    <a href="{{url('/dashboard')}}" class="btn btn-icon btn-icon-start btn-primary mt-3 stretched-link">
                                    <i data-cs-icon="chevron-right"></i>
                                    <span>Crypto Clicks Room</span>
                                    </a>
                                 </div>

                                 @endguest
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-lg-4 mb-5">
                           <div class="scroll-out">
                              <div class="scroll-by-count" data-count="4">
                                 <div class="card mb-2 hover-border-primary">
                                    <a href="{{url('pay')}}" class="row g-0 sh-9">
                                       <div class="col-auto">
                                          <div class="sw-9 sh-9 d-inline-block d-flex justify-content-center align-items-center">
                                             <div class="fw-bold text-primary">
                                                <i data-cs-icon="dollar"></i>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col">
                                          <div class="card-body d-flex flex-column ps-0 pt-0 pb-0 h-100 justify-content-center">
                                             <div class="d-flex flex-column">
                                                <div class="text-alternate">Deposit</div>
                                                <div class="text-small text-muted">Fund Account Level.</div>
                                             </div>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                                 <div class="card mb-2 hover-border-primary">
                                    <a href="{{url('withdraw')}}" class="row g-0 sh-9">
                                       <div class="col-auto">
                                          <div class="sw-9 sh-9 d-inline-block d-flex justify-content-center align-items-center">
                                             <div class="fw-bold text-primary">
                                                <i data-cs-icon="cloud-download"></i>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col">
                                          <div class="card-body d-flex flex-column ps-0 pt-0 pb-0 h-100 justify-content-center">
                                             <div class="d-flex flex-column">
                                                <div class="text-alternate">Withdrawal</div>
                                                <div class="text-small text-muted">Withdraw Cash.</div>
                                             </div>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                                 <div class="card mb-2 hover-border-primary">
                                    <a href="{{url('referrals')}}" class="row g-0 sh-9">
                                       <div class="col-auto">
                                          <div class="sw-9 sh-9 d-inline-block d-flex justify-content-center align-items-center">
                                             <div class="fw-bold text-primary">
                                                <i data-cs-icon="shield"></i>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col">
                                          <div class="card-body d-flex flex-column ps-0 pt-0 pb-0 h-100 justify-content-center">
                                             <div class="d-flex flex-column">
                                                <div class="text-alternate">Referral Bonus</div>
                                                <div class="text-small text-muted">Check your team and bonuses.</div>
                                             </div>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                                 @guest

                                 @else
                                 <div class="card mb-2 hover-border-primary">
                                    <div class="row g-0 sh-9">
                                       <div class="col-auto">
                                          <div class="sw-9 sh-9 d-inline-block d-flex justify-content-center align-items-center">
                                             <div class="fw-bold text-primary">
                                                <i data-cs-icon="file-text"></i>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col">
                                          <div class="card-body d-flex flex-column ps-0 pt-0 pb-0 h-100 justify-content-center">
                                             <div class="d-flex flex-column">
                                                <div class="text-alternate">{{$referralink}}</div>
                                                <div class="text-small text-muted"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endguest
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="mb-5">
                        <h2 class="small-title">Account Levels</h2>
                        <div class="row g-2 row-cols-1 row-cols-xl-2 row-cols-xxl-4">
                          
                             @foreach ($categories as $category)
                           <div class="col">
                              <div class="card h-100">
                                 <div class="card-body">
                                     @foreach ($category->children as $sub)
                                    <div class="text-center">
                                       <a href="{{ route('listings.index', [$sub]) }}"><img src="/img/illustration/{{$sub->id}}.jpg" width="200" height="200" class="theme-filter" alt="launch"></a>
                                      
                                       <div class="d-flex flex-column sh-5">
                                          <h3><a style="color:green;" href="{{ route('listings.index', [$sub]) }}" class="heading ">{{ $category->name }} Account</a></h3>
                                       </div>
                                       
                                       @endforeach
                                        
                                        <h3 class="" style="font-color:red;" >Deposit K{{$category->percent}}</h3>
                                         @foreach ($category->children as $sub)
                                         <div class="d-flex flex-column sh-5">
                                          <a href="{{ route('listings.index', [$sub]) }}" class="heading">Earn K{{ $sub->begin }} - K{{ $sub->end }} Per Click</a>
                                       </div>

                                        @if (Auth::user()->accounttype!=$sub->id)
                                           <a href="{{ url('pay')}}" class="btn btn-icon btn-icon-start btn-warning mt-3 ">                                                                                                                                                      
                          <span>GET THIS PACKAGE</span>
                                    </a>
                                    <h3 class="" style="color:red;" ><i data-cs-icon="lock-on" class="unpin" data-cs-size="18"></i> LOCKED</h3>
                                    @else
                                    <h3 class="" style="color:green;" >OPEN</h3>
                                    <a href="{{ route('listings.index', [$sub]) }}" class="btn btn-icon btn-icon-start btn-success mt-3 ">                                                                                                                                                      
                          <span>START EARNING</span>
                                    </a>
                                    @endif
                                       <h3 class="" style="color:purple;" >{{$sub->limit}} MAXIMUM CLICKS</h3>
                                   
                                       @endforeach
                                    </div>
                                    <div class="text-muted">You can request an upgrade anytime.</div>
                                   
                                 </div>
                              </div>
                           </div>
                           @endforeach
                          

                        </div>
                     </div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection