 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 
@if($listing->recommit())

               <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Bonus Withdrawn</h1>
                              <div class="text-muted font-heading text-small">Success</div>
                           </div>
                        </div>
                     </div>
                     <h2 class="small-title"></h2>
                                  
<div class="row">
<div class="col-12 col-lg-4 mb-5">
<h2 class=""  style="color:green;">{{ Auth::user()->area->unit }} {{$listing->amount}}</h2>
<div class="card">
<div class="card-body sh-45 d-flex align-items-center justify-content-center">
<div class="text-center">

<hr>
<div class="cta-3">Bonus Withdrawal Time is 5Hours</div>
 

<a href="{{ url('refferals') }}" class="btn btn-warning">Back to Bonus</a>
</div>
</div>
</div>
</div>





@else
@if(!$listing->matched())

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Cash Withdrawal</h1>
                              <div class="text-muted font-heading text-small">Success</div>
                           </div>
                        </div>
                     </div>
                     <h2 class="small-title"></h2>
                                  
<div class="row">
<div class="col-12 col-lg-4 mb-5">
<h2 class=""  style="color:green;">K {{$listing->extamount}}</h2>
<div class="card">
<div class="card-body sh-45 d-flex align-items-center justify-content-center">
<div class="text-center">
<hr>
<h2>Congrats! you have withdrawn from your account, expect it shortly.</h2>

<a href="{{url('dashboard')}}" class="btn btn-warning">Upgrade Package and get more clicks</a>
</div>
</div>
</div>
</div>
@else

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Congratulations, you promoted a crypto below</h1>
                              <div class="text-muted font-heading text-small">Success</div>
                           </div>
                        </div>
                     </div>
                     <h2 class="small-title"></h2>
                                  
<div class="row">
<div class="col-12 col-lg-4 mb-5">
<h2 class=""  style="color:green;">R{{$listing->amount}} Earned!!</h2>
<div class="card">
<div class="card-body sh-45 d-flex align-items-center justify-content-center">
<div class="text-center">
<img src="/img/product/{{random_int(1,50)}}.png" height="100" width="100" class="theme-filter" alt="access">
<hr>
<div class="cta-3">Click Serial :00{{$listing->vip}}</div>

<div class="cta-5 text-primary mb-4">Only {{$listing->category->cpd}} Clicks per day</div>
<a href="{{ URL::previous() }}" class="btn btn-warning">Go Back to Click Room</a>
</div>
</div>
</div>
</div>


@endif
 @endif                   
                  </div>
               </div>
            </div>
         </main>

 
@endsection