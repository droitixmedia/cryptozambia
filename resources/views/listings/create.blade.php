 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Deposit</h1>
                              <div class="text-muted font-heading text-small">Account</div>
                           </div>
                        </div>
                     </div>
                      
                   @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               
 

                         @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach


                      <h2 class="small-title">Account Balance R{{$sum}}</h2>
                     <div class="card mb-5 col-lg-6">
                        <div class="card-body ">
                          
                                       <form action="{{ route('listings.store', [$area]) }}" method="post">
                                        <div class=" col-lg-12">
                              @include('listings.partials.forms._categories')
                                        </div>

                                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                            <label>Amount</label>
                                            <input type="text" name="amount" class="form-control">

                                              @if ($errors->has('amount'))
                                <span class="help-block">
                                    {{ $errors->first('amount') }}
                                </span>
                            @endif
                                        </div>
                                        <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                            <button type="submit" class="btn btn-primary">Create</button>

                                         {{ csrf_field() }}
                                    </form>

                          
                        </div>
                     </div>

                     
                  </div>
               </div>
            </div>
         </main>

 
@endsection