 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                  <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">{{$category->parent->name}} Clicks Room</h1>
                              <div class="text-muted font-heading text-small"></div>
                           </div>
                        </div>
                     </div><hr>
                     @if (Auth::user()->accounttype!=$category->id)
                       
<div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">   
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">{{$category->name}} Package</h1>
                              <div class="text-muted font-heading text-small">Click on crypto orders in this account</div>
                           </div>
                        </div>
                     </div><hr>
                     <div class="card mb-5 col-lg-6" >
                        <div class="card-body sh-50 d-flex align-items-center justify-content-center">
                           <div class="text-center">
                              <img src="/img/illustration/{{$category->id}}.jpg" height="200" width="200"  class="theme-filter" alt="launch"><hr>
                              <div class="cta-1"> <i data-cs-icon="lock-on" class="unpin" data-cs-size="18"></i> LOCKED</div><hr>
                              <div class="cta-3 text-primary mb-4"></div>
                              <a href="{{ url('pay')}}" class="btn btn-danger btn-lg ">  Deposit K{{$category->parent->percent}}   to Access this Package</a><br><br><br><br>
                           </div>
                        </div>
                     </div>
           
                  </div>

                     @else

                                           
<div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Promote on {{$category->name}} Level</h1>
                              <div class="text-muted font-heading text-small">Earnings are based on the account level</div>
                           </div>
                        </div>
                     </div><hr>
                     <div class="card mb-5 col-lg-6" >
                        <div class="card-body sh-50 d-flex align-items-center justify-content-center">
                           <div class="text-center">
                              <img src="/img/illustration/{{$category->id}}.jpg" height="200" width="200"  class="theme-filter" alt="launch"><br><br>
                               @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               
 

                         @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                          @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if(!$listing->type())
                            
                               
 

                         @php ($withdrawn += $listing->extamount)

                           @if ($loop->last)

                           @endif

                           @else

                       
                         @endif
                     @endforeach
                     @php($balance=$sum-$withdrawn)
             @if ($clicks->count() >= $category->limit) 
             @if($balance ==0)
<div class="cta-1">YOUR WITHDRAWAL HAS BEEN RECIEVED, UPGRADE YOUR ACCOUNT NOW AND EARN MORE </div>
             @else
<div class="cta-1">YOUR FREE CLICKS ARE FINISHED, UPGRADE TO THE NEXT PACKAGE </div>
@endif
                     @if($balance ==0)
                     <a href="#" class="btn btn-success"><span>Withdrawal sent......</span><i class="os-icon os-icon-grid-18"></i></a><br>
<a href="{{url('upgrade')}}" class="btn btn-danger"><span>Upgrade Account</span><i class="os-icon os-icon-grid-18"></i></a>
                     @else
<a href="{{url('upgrade')}}" class="btn btn-success"><span>CONGRATS YOU EARNED K{{$sum-$withdrawn}} FROM YOUR {{$category->name}} ACCOUNT, UPGRADE TO NEXT PACKAGE TO BE ABLE TO WITHDRAW</span><i class="os-icon os-icon-grid-18"></i></a>
                    @endif

             @else  
                             <div style="color:purple;" class="cta-1">You have done {{$limit->count()}} Clicks Today</div><hr>
                              
                     

                              <hr>
                              <div class="cta-1">Earn From K{{$category->begin}} - K{{$category->end}} per order</div><hr>
                              <div class="cta-1"> </div><hr>
                              <div class="cta-3 text-primary mb-4"></div>
                              <form class="form" method="post" action="{{ route('listings.store', [$area]) }}">
                                   
                                    <div class="row">
                           
                          
                              
                             


                              <input type="hidden" class="form-control" name="name" id="value" value="star VIP">
                              <input type="hidden" class="form-control" name="descrep" id="value" value="descrep">
                              <input type="hidden" class="form-control" name="pclink" id="value" value="descrep">

                                       <input type="hidden" class="form-control" name="amount" id="value" value="{{$category->parent->price}}">
                                       <input type="hidden" class="form-control" name="extamount" id="value" value="500">
                                       <input type="hidden" class="form-control" name="serial" id="value" value="11111111">
                                       <input type="hidden" class="form-control" name="vip" id="value" value="1">
                                       <input type="hidden" class="form-control" name="matched" id="matched" value="1">
                                       <input type="hidden" class="form-control" name="value" id="value" value="0.045">
                                       <input type="hidden" class="form-control" name="period" id="period" value="28">
                                       <input type="hidden" class="form-control" name="current" id="current" value="0">
                                       <input type="hidden" class="form-control" name="recommit" id="period" value="0">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="category" value="{{$category->id}}">
                                    </div>
                                    

                                
                                    
                           @if ($clicks->count() < $category->limit)     
                                    <div class="form-buttons-w text-right compact">
                                       @if ($limit->count() < $category->cpd)
                              <button class="btn btn-primary" type="submit" ><span>Load Crypto Clicks</span><i class="os-icon os-icon-grid-18"></i></button>
                           @else
                             <a class="btn btn-danger"><span>Limit Reached for Today</span><i class="os-icon os-icon-grid-18"></i></a><br><br>

                             <a href="{{url('upgrade')}}" class="btn btn-success"><span>Upgrade to PAID ACCOUNT</span><i class="os-icon os-icon-grid-18"></i></a>
                           @endif

                           @else
                            <a class="btn btn-secondary"><span>{{$clicks->count()}}</span><i class="os-icon os-icon-grid-18"></i></a>


                           @endif
                     </div>
                                    {{ csrf_field() }} 
                                 </form>
                          <br><br><br>
                           </div>
                           @endif
                        </div>
                     </div>
           
                  </div>
@endif
                  </div>
            </div>
         </main>
<script type="text/javascript">
   
   $(document).ready(function() {
    
  var audio  = new Audio('https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/click.mp3');
  var audio2 = new Audio('https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/clickUp.mp3')

  $(".button").mousedown(function() {
    audio2.load();
    audio2.play();
  });
   
  $(".button").mouseup(function() {
    audio.load();
    audio.play();
  });
});
</script>
 
@endsection