 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Update Your Profile</h1>
                              <div class="text-muted font-heading text-small">Profile</div>

                  <a href="{{url('/change-password')}}" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    <i data-cs-icon="plus"></i>
                                    <span>Change Password</span>
                                    </a>
                           </div>
                        </div>
                     </div>
                     <h2 class="small-title"></h2>
                     <div class="card mb-5 ">
                        <div class="card-body ">
                               @if (session()->has('impersonate'))
                                   <form class="form" method="POST" action="{{route('profile.update')}}">
                        @csrf

                            <div class="box-body">
                                <h5 class="box-title mb-0 text-info"><i class="ti-user me-15"></i> Personal Info</h5>
                                <hr class="my-15">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">First Name</label>
                                      <input type="text"  name="name" value="{{$user->name}}" class="form-control" placeholder="First Name">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Last Name</label>
                                      <input type="text"  name="surname" value="{{$user->surname}}" class="form-control" placeholder="Last Name">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">E-mail</label>
                                      <input type="text"  name="email" value="{{$user->email}}" class="form-control" placeholder="E-mail">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Contact Number</label>
                                      <input type="text"  name="phone_number" value="{{$user->phone_number}}" class="form-control" placeholder="Phone">
                                    </div>
                                  </div>
                                </div><br>
                               <h5 class="box-title mb-0 text-info"><i class="ti-credit-card me-15"></i> MOBILE MONEY DETAILS</h5>
                                <hr class="my-15">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">MOBILE MONEY</label>
                                      <input type="text"  name="bank" value="{{$user->bank}}" class="form-control" placeholder="Bank">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">MOBILE MONEY NUMBER</label>
                                      <input type="text"  name="account" value="{{$user->account}}" class="form-control" placeholder=" Number">
                                    </div>
                                  </div>
                                </div><br>
                                 <h5 class="box-title mb-0 text-info"><i class="ti-credit-card me-15"></i> Other Payment Methods</h5>
                                <hr class="my-15">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Bitcoin Address</label>
                                      <input type="text"  name="btcaddress" value="{{$user->btcaddress}}" class="form-control" placeholder="Bitcoin Address">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Paypal</label>
                                      <input type="text"  name="branch" value="{{$user->branch}}" class="form-control" placeholder="Paypal">
                                    </div>
                                  </div>
                                  <hr>
                               
                                   <input class="form-control" name="accounttype" value="{{$user->accounttype}}">
                                  <input type="hidden" class="form-control" name="phone" value="phone">
                                  
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                  <i class="fa fa-paper-plane"></i> Update Details
                                </button> 
                        </form>
                                  @endif
                           <form class="form" method="POST" action="{{route('profile.update')}}">
                        @csrf

                            <div class="box-body">
                                <h5 class="box-title mb-0 text-info"><i class="ti-user me-15"></i> Personal Info</h5>
                                <hr class="my-15">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">First Name</label>
                                      <input type="text"  name="name" value="{{$user->name}}" class="form-control" placeholder="First Name">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Last Name</label>
                                      <input type="text"  name="surname" value="{{$user->surname}}" class="form-control" placeholder="Last Name">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">E-mail</label>
                                      <input type="text"  name="email" value="{{$user->email}}" class="form-control" placeholder="E-mail">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Contact Number</label>
                                      <input type="text"  name="phone_number" value="{{$user->phone_number}}" class="form-control" placeholder="Phone">
                                    </div>
                                  </div>
                                </div><br>
                               <h5 class="box-title mb-0 text-info"><i class="ti-credit-card me-15"></i> BANK ACCOUNT DETAILS</h5>
                                <hr class="my-15">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Bank Name</label>
                                      <input type="text"  name="bank" value="{{$user->bank}}" class="form-control" placeholder="Mobile Money Name">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Account Number</label>
                                      <input type="text"  name="account" value="{{$user->account}}" class="form-control" placeholder="Mobile money number">
                                    </div>
                                  </div>
                                </div><br>
                                 <h5 class="box-title mb-0 text-info"><i class="ti-credit-card me-15"></i> Other Payment Methods</h5>
                                <hr class="my-15">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Bitcoin Address</label>
                                      <input type="text"  name="btcaddress" value="{{$user->btcaddress}}" class="form-control" placeholder="Bitcoin Address">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Paypal</label>
                                      <input type="text"  name="branch" value="{{$user->branch}}" class="form-control" placeholder="Paypal">
                                    </div>
                                  </div>
                                  <hr>
                               
                                   <input type="hidden" class="form-control" name="accounttype" value="{{$user->accounttype}}">
                                  <input type="hidden" class="form-control" name="phone" value="phone">
                                  
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                  <i class="fa fa-paper-plane"></i> Update Details
                                </button> 
                        </form>
                        </div>
                     </div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection