@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 


             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">   
                                <ul>
                                    <li>Home</li>
                                    <li>Withdrawals</li> 
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                                      <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="transactions-table">
                                <h3 class="title">
          
                                </h3><br><br>
                                <div class="table-responsive">
                                            <table class="table table-centered table-nowrap">
                                                <thead>
                                                    <tr>

                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Mobile Money</th>
                                                        <th>Email</th>
                                                        <th>Amount</th>
                                                        <th>Contact</th>
                                                        
                                                      
                                                        
                                                        <th>Creation Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                      @foreach ($listings as $listing)
                                              


                                        <tr>
                                           
                                           
                   
                                 @if (!$listing->type())
                                           <tr>
                                            <td>{{$listing->user->id}}</td>
                                                <td>{{$listing->user->name}} {{$listing->user->surname}}</td>
                                                <td>{{$listing->user->account}}</td>
                                                <td>{{$listing->user->email}}</td>
                                                
                                                <td>{{$listing->extamount}}</td>
                                                   <td>{{$listing->phone_number}}</td>
                                                <td>{{$listing->created_at}}</td>
                                           
                                            <td>
                                              <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('comment-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Reallocate Coins"><i class="fe fe-trash"></i>Reallocate</a></li>

                             <form action="{{route('admin.listing.destroy', [$listing->id])}}" method="post" id="comment-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>


                                            </td>
                                             </tr>
                                             @endif
                             



                                        @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                            </div>
                        </div>
                    </div>     
                        
        


                   
                </div>
            </div>
            <!-- account end -->

@endsection
