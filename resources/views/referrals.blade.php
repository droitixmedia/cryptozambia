 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Bonus Page</h1>
                              <div class="text-muted font-heading text-small">Bonuses</div>
                           </div>
                        </div>
                     </div>
                      <form action="{{ route('listings.store', [$area]) }}" method="post" class="buysell-form">
                     @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach

                     <h2 class="small-title">
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                        <input type="hidden" class="form-control" name="matched" id="value" value="0">
                                          <input type="hidden" class="form-control" name="value" id="value" value="1">
                                           <input type="hidden" class="form-control" name="current" id="value" value="1">
                                         <input type="hidden" class="form-control" name="recommit" id="period" value="1">
                                         <input type="hidden" class="form-control" name="period" id="period" value="1">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                            
                                        
                                           {{ csrf_field() }}

                                    </form><!-- .buysell-form --><br>
                      <div class="table-responsive">
                              <table class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>Name</th>
                              
                                       <th>Contact</th>
                                       <th class="text-center">Bonus Gained</th>
                                       <th class="text-right">Email</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach ($referrals as $ref)
                                    <tr>
                                       <td>{{$ref->id}}<div class="logged-user-w">
                        <div class="logged-user-i"> <div class="avatar-w"><img alt="" src="img/avatar1.jpg"></div></div></div></td>
                                       <td>{{$ref->name}} {{$ref->surname}}</td>
                                        <td>{{$ref->phone_number}}</td>
                                        
                                        <td>@if($ref->accounttype == 2)
                                              K0
                                              @elseif($ref->accounttype == 4)
                                              K15
                                              @elseif($ref->accounttype == 6)
                                              K50
                                               @elseif($ref->accounttype == 8)
                                              K75
                                               @elseif($ref->accounttype == 10)
                                              K100
                                               @elseif($ref->accounttype == 12)
                                              K125
                                               @elseif($ref->accounttype == 14)
                                             K150
                                               @elseif($ref->accounttype == 16)
                                              K250
                                              @endif
                                        </td>
                                       
                                       
                                       
                                       <td class="text-right">{{$ref->email}}</td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                              {{ $referrals->links() }}
                           </div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection