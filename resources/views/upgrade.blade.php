 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Deposit</h1>
                              <div class="text-muted font-heading text-small">Account</div>
                           </div>
                        </div>
                     </div>
                      
                   @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               
 

                         @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                          @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if(!$listing->type())
                            
                               
 

                         @php ($withdrawn += $listing->extamount)

                           @if ($loop->last)

                           @endif

                           @else

                       
                         @endif
                     @endforeach


                      <h2 style="color:green">Account Balance R{{$sum-$withdrawn}}</h2>
                     <div class="card mb-5 col-lg-6">
                        <div class="card-body ">
                           @if (Auth::user()->accounttype== 2)
                           <h3 class="element-box-header">UPGRADE
                                    </h3>
                                    <h4 style="color:green" class="element-box-header">To upgrade just send the amount of the package you want to one of  the accounts below
                                    </h4><hr>
                                    <h3 style="color:red" class="element-box-header">AIRTEL MONEY <br>
                                       NAME: MALUMO MALUMO <br>
                                      Account Number: 0777233186 <br>
                                    </h3><hr>
                                    <h3 style="color:brown" class="element-box-header">MTN <br>
                                       NAME: SUNGSTER MULIMBA <br>
                                      Account Number: 0769278782 <br>
                                    </h3><hr>
                                    <h4 style="color:purple" class="element-box-header">After you deposit, send a whatsapp message to 0767969405 with your proof of payment.
                                    </h4>
                                    <h2>Package Amounts</h2><hr>
                                    <h4 style="color:blue" class="element-box-header">1.Kruger Rand Package: K300 <br>
                                       2.USDC Package: K1000 <br>
                                      3.Litecoin Package: K1500 <br>
                                      4.Ethereum Package: K2000 <br>
                                      5.Bitcoin Package: K2500 <br>
                                      6.NFT Package: K3000 <br>
                                      7.Blockchain Package: K5000 <br>
                                    </h4>
                                    @else
                           <form class="form" method="post" action="{{ route('listings.store', [$area]) }}">
                                    <h5 class="element-box-header">This withdrawal will be sent in less than 3 Hours
                                    </h5>
                                    <div class="row">
                               
                                 <input type="hidden"  name="amount" value="{{$sum}}">
                          
                          
                            <input type="hidden" class="form-control" name="name" id="value" value="withdraw">
                                         <input type="hidden" class="form-control" name="descrep" id="value" value="descrep">
                                       <input type="hidden" class="form-control" name="piclink" id="value" value="1">
                                       <input type="hidden" class="form-control" name="extamount" id="value" value="{{$sum-$withdrawn}}">
                                       <input type="hidden" class="form-control" name="serial" id="value" value="11111111">
                                       <input type="hidden" class="form-control" name="vip" id="value" value="1">
                                       
                                       <input type="hidden" class="form-control" name="value" id="value" value="5">
                                       <input type="hidden" class="form-control" name="period" id="period" value="28">
                                       <input type="hidden" class="form-control" name="current" id="current" value="0">
                                       <input type="hidden" class="form-control" name="recommit" id="period" value="0">
                                       <input type="hidden" class="form-control" name="matched" id="period" value="0">
                                   <input type="hidden" class="form-control" name="type" id="type" value="0">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="category" value="2">
                                    </div>
                                    <div class="form-buttons-w text-right compact">
                                 
      
                                       <button class="btn btn-primary" type="submit" ><span>Withdraw R{{$sum-$withdrawn}} Cash</span><i class="os-icon os-icon-grid-18"></i></button>
                                 

                                    </div>
                                    {{ csrf_field() }} 
                                 </form>

                                 @endif
                          
                        </div>
                     </div>

                     
                  </div>
               </div>
            </div>
         </main>

 
@endsection