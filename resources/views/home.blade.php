<!DOCTYPE html>
<html lang="en" xml:lang="en">    

<head>       
    <meta charset="UTF-8">  
    <!-- Responsive Meta -->                    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- favicon & bookmark -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"  href="https://sacredthemes.net/coinpool/images/bookmark.png" type="image/x-icon" />
    <link rel="shortcut icon" href="https://sacredthemes.net/coinpool/images/favicon.ico" type="image/x-icon" /> 
    <!-- Font Family -->
    <link href="../../fonts.googleapis.com/csscd33.css?family=PT+Sans:400,700" rel="stylesheet"> 
    <link href="../../fonts.googleapis.com/css6618.css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="../../fonts.googleapis.com/css9425.css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">
    <!-- Website Title -->
    <title>Click Crypto | Start</title>
    <!-- Stylesheets Start -->
    <link rel="stylesheet" href="https://sacredthemes.net/coinpool/css/fontawesome.min.css" type="text/css"/>
    <link rel="stylesheet" href="https://sacredthemes.net/coinpool/css/bootstrap.css" type="text/css"/>
    <link rel="stylesheet" href="https://sacredthemes.net/coinpool/css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="https://sacredthemes.net/coinpool/css/owl.carousel.min.css" type="text/css"/>
    <link rel="stylesheet" href="https://sacredthemes.net/coinpool/style.css" type="text/css"/>
    <link rel="stylesheet" href="https://sacredthemes.net/coinpool/css/responsive.css" type="text/css"/>
     <!-- Stylesheets End -->
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->

    
</head>
<body>
    <!--Main Wrapper Start-->
    <div class="wrapper" id="top">

        <!--Header Start -->
        <header>
            <div class="container">
                <script defer src="https://www.livecoinwatch.com/static/lcw-widget.js"></script> <div class="livecoinwatch-widget-5" lcw-base="ZMW" lcw-color-tx="#999999" lcw-marquee-1="coins" lcw-marquee-2="movers" lcw-marquee-items="30" ></div>
                <div class="row">
                    <div class="col-sm-6 col-md-4 logo">
                        <a href="{{url('/')}}" title="Crypto Clicks">
                            <img class="light" width="100" height="50" src="/img/logo/dailytasks.png" alt="Crypto Clicks">
                            <img class="dark"  width="100" height="50"  src="/img/logo/dailytasks.png" alt="Crypto Clicks">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-8 main-menu">
                        <div class="menu-icon">
                          <span class="top"></span>
                          <span class="middle"></span>
                          <span class="bottom"></span>
                        </div>
                        <nav class="onepage">
                            <ul>
                                <li class="active"><a href="#top">Home</a></li>
                                <li><a href="#about">About Crypto Clicks</a></li>
                                <li><a href="#token">Package Sizes</a></li>
                              @guest
                                <li class="nav-btn"><a href="{{url('login')}}">Sign In</a></li>
                                @else
                                 <li class="nav-btn"><a href="{{url('dashboard')}}">Dashboard</a></li>
                                @endguest
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!--Header End-->

        <!-- Content Section Start -->   
        <div class="midd-container">
            <!-- Hero Section Start -->   
            <div class="hero-main mercury-layout white-sec" style="background:url(https://sacredthemes.net/coinpool/images/banner-6.jpg);">
                <div class="container">
                    <div class="row align-items-center flex-row-reverse">
                        <div class="col-sm-12 col-md-6" data-wow-delay="0.5s">
                            <div class="mercury-animation">
                                <div class="numbers">
                                    <div class="number-one"></div>
                                    <div class="number-two"></div>
                                </div>
                                <div id="earth-box-cover">
                                    <div class="earth-icons">
                                        <i class="icon-1"></i>
                                        <i class="icon-2"></i>
                                        <i class="icon-3"></i>
                                        <i class="icon-4"></i>
                                        <i class="icon-5"></i>
                                        <i class="icon-6"></i>
                                        <i class="icon-7"></i>
                                    </div>
                                    <div id="earth-box">
                                        <span></span>
                                    </div>
                                </div>
                                <!-- <div class="earth">
                                    <img src="images/mercury-earth.png" alt="">
                                </div> -->
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <h1>Earn daily with crypto clicks</h1>
                            <p class="lead">Crypto clicks is a revolutionary way of earning while promoting cryptocurrencies.</p>
                            <div class="hero-btns">
                                <a href="{{url('dashboard')}}" class="btn">Deposit Package</a>
                                @guest
                                <a href="{{url('register')}}" class="btn btn3">Create Account</a>
                                @else
                                 <a href="{{url('dashboard')}}" class="btn btn3">Dashboard</a>
                                @endguest
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <!--About Start -->
            <div class="about-section p-tb mercury-layout" id="about">
                <div class="container">
                    <div class="row flex-row-reverse align-items-center">
                        <div class="col-lg-6 col-md-12">
                            <div class="about-mercury-img mobile-visible">
                                <img src="https://sacredthemes.net/coinpool/images/about-mercury-img-with-coin.png" alt="Crypto Clicks">
                            </div>
                            <div class="about-mercury-animation mobile-hidden">
                                <div class="coin-animation">
                                    <i class="coin coin-1"></i>
                                    <i class="coin coin-2"></i>
                                    <i class="coin coin-3"></i>
                                    <i class="coin coin-4"></i>
                                    <i class="coin coin-5"></i>
                                    <i class="coin coin-6"></i>
                                    <i class="coin coin-7"></i>
                                    <i class="coin coin-8"></i>
                                    <i class="coin coin-9"></i>
                                </div>
                                <img class="mercury-base" src="https://sacredthemes.net/coinpool/images/about-mercury-img.png" alt="About Mercury">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <h2 class="section-heading">About Crypto Clicks</h2>
                            <h4>Why earn with Crypto Clicks</h4>
                           
                           
                        </div>
                       
                    </div>
                </div>
            </div>
            <!--About end -->
          
            <!-- Benefits Start -->
            <div class="benefits p-tb light-gray-bg mercury-layout">
                <div class="container">
                    <div class="text-center"><h2 class="section-heading">Benefits of Using Crypto Clicks</h2></div>
                    <div class="sub-txt mw-850 text-center">
                        <p>Crypto Clicks is a revolutionary way of making money, at the same time advertising and promoting upcoming cryptocurrencies</p>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="benefit-box text-center">
                                <div class="benefit-icon">
                                    <img src="https://sacredthemes.net/coinpool/images/benefit-icon-1.png" alt="Safe and Secure">
                                </div>
                                <div class="text">
                                    <h4>Safe and Secure</h4>
                                    <p>Crypto Clicks is directly linked to the blockchain and rides on the blockchain achitecture, that is why its safe and secure.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="benefit-box text-center">
                                <div class="benefit-icon">
                                    <img src="https://sacredthemes.net/coinpool/images/benefit-icon-2.png" alt="Instant Exchange">
                                </div>
                                <div class="text">
                                    <h4>Instant Exchange</h4>
                                    <p>As an instant exchange , customers are enabled to perform transactions without delays and low latency.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="benefit-box text-center">
                                <div class="benefit-icon">
                                    <img src="https://sacredthemes.net/coinpool/images/benefit-icon-3.png" alt="World Coverage">
                                </div>
                                <div class="text">
                                    <h4>World Coverage</h4>
                                    <p>Crypto Clicks is worldwide and allows anyone anywhere to work hard and make money</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="benefit-box text-center">
                                <div class="benefit-icon">
                                    <img src="https://sacredthemes.net/coinpool/images/benefit-icon-4.png" alt="Mobile Apps">
                                </div>
                                <div class="text">
                                    <h4>Mobile Apps</h4>
                                    <p>All our upcoming mobile apps will make it easy to promote crypto and make money.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="benefit-box text-center">
                                <div class="benefit-icon">
                                    <img src="https://sacredthemes.net/coinpool/images/benefit-icon-5.png" alt="Strong Network">
                                </div>
                                <div class="text">
                                    <h4>Strong Network</h4>
                                    <p>Crypto Clicks is anchored on a strong network of dedicated users and organisations, who have the system at heart.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="benefit-box text-center">
                                <div class="benefit-icon">
                                    <img src="https://sacredthemes.net/coinpool/images/benefit-icon-6.png" alt="Margin Trading">
                                </div>
                                <div class="text">
                                    <h4>Margin Trading</h4>
                                    <p>Well connected compensation system, allowing flowless renumeration and pay for work done.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- Benefits End -->
            <!-- Token Sale start -->
            <div class="token-sale p-tb mercury-layout" id="token">
                <div class="container">
                    <div class="text-center"><h2 class="section-heading">Packages and sizes</h2></div>
                    <div class="sub-txt mw-650 text-center">
                        <p>Crypto Clicks is free but for one to make a proper income , they have to join in with a package that matches their desired money outcome.</p>
                    </div>
                    <div class="row flex-row-reverse align-items-center">
                        <div class="col-lg-7 col-md-12">
                            <div class="token-allocation-animation">
                                <div class="main-circle-one">
                                    <ul class="allocation-list-point">
                                        <li><i class="point"></i><span>K 0 Starter Package</span></li>
                                        <li><i class="point"></i><span>K300 Kruger Rand Package</span></li>
                                        <li><i class="point"></i><span>K1000 USDC Package</span></li>
                                        <li><i class="point"></i><span>K1500 Litecoin Package</span></li>
                                        <li><i class="point"></i><span>K2000 Ethereum Package</span></li>
                                        <li><i class="point"></i><span>K2500 Bitcoin Package</span></li>
                                        <li><i class="point"></i><span>K3000 NFT Package</span></li>
                                        <li><i class="point"></i><span>K5000 Blockchain Package</span></li>
                                       
                                    </ul>
                                    <div class="center-point">
                                        <img src="https://sacredthemes.net/coinpool/images/gold-animation-icon.png" alt="" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-12">
                            <ul class="allocation-list">
                                <li><i class="point"></i><span>K 0 Starter Package</span></li>
                                        
                                        <li><i class="point"></i><span>K300 Kruger Rand Package</span></li>
                                        <li><i class="point"></i><span>K1000 USDC Package</span></li>
                                        <li><i class="point"></i><span>K1500 Litecoin Package</span></li>
                                        <li><i class="point"></i><span>K2000 Ethereum Package</span></li>
                                        <li><i class="point"></i><span>K2500 Bitcoin Package</span></li>
                                        <li><i class="point"></i><span>K3000 NFT Package</span></li>
                                        <li><i class="point"></i><span>K5000 Blockchain Package</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Token Sale end -->
           
        </div>
        <!-- Content Section End -->   
        <div class="clear"></div>
        <!--footer Start-->   
        <footer class="footer-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 footer-box-1">
                        <div class="footer-logo">
                            <img class="dark"  width="150" height="100"  src="/img/logo/dailytasks.png" alt="Crypto Clicks">
                        </div>
                        <p>Crypto Clicks is a revolutionary way of earning while promoting upcoming cryptocurencies</p>
                    </div>
                    <div class="col-md-3 footer-box-2">
                        <ul class="footer-menu onepage">
                            <li><a href="#about">About Crypto Clicks</a></li>
                           
                            <li><a href="#token">Account Sizes</a></li>
                           
                        </ul>
                    </div>
                    <div class="col-md-5 footer-box-3">
                        <h4>Subscribe</h4>
                        <p>Keep up to date with our progress. Subscribe for e-mail updates.</p>
                        <div class="newsletter">
                            <form>
                                <div class="input"><input type="email" name="Email" placeholder="Your email address"></div>
                                <div class="submit"><input class="btn" type="submit" name="subscribe" value="subscribe"></div>
                            </form>
                        </div>
                        <div class="socials">
                            <ul>
                                <li><a href="https://facebook.com/"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com/"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://plus.google.com/"><i class="fab fa-google-plus-g"></i></a></li>
                                <li><a href="https://www.youtube.com/"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="copyrights style-1">
                            © 2022 Crypto Clicks  Developed by <a href="#">Crypto Clicks</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--footer end--> 
    </div>
    <!--Main Wrapper End-->
 
    <script src="https://sacredthemes.net/coinpool/js/jquery.min.js"></script>
    <script src="https://sacredthemes.net/coinpool/js/bootstrap.min.js"></script>
    <script src="https://sacredthemes.net/coinpool/js/onpagescroll.js"></script>
    <script src="https://sacredthemes.net/coinpool/js/wow.min.js"></script>
    <script src="https://sacredthemes.net/coinpool/js/jquery.countdown.js"></script>
    <script src="https://sacredthemes.net/coinpool/js/owl.carousel.js"></script>
    <script src="https://sacredthemes.net/coinpool/js/script.js"></script>

  

    <script type="text/javascript">
        function isScrolledIntoView(elem) {
            var docViewTop = jQuery(window).scrollTop();
            var docViewBottom = docViewTop + jQuery(window).height();
            var elemTop = jQuery(elem).offset().top;
            var elemBottom = elemTop + jQuery(elem).height();
            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }
        jQuery(window).scroll(function () {
            jQuery('.about-mercury-animation').each(function () {
                if (isScrolledIntoView(this) === true) {
                    jQuery(this).addClass('visible');
                }
            });
            jQuery('.token-pricing-section').each(function () {
                if (isScrolledIntoView(this) === true) {
                    jQuery(this).addClass('visible');
                }
            });
        });
        jQuery(document).ready(function () {
            jQuery('.allocation-list-point li:first-child').addClass('hover');
            jQuery('.allocation-list-point li .point').hover(function(){
                jQuery('.allocation-list-point li').removeClass('hover');
                jQuery(this).parent('li').addClass('hover');
            });
        });
    </script>
   <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/620b74881ffac05b1d79f027/1frubuef1';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>

</html>