@extends('layouts.regapp')
@section('title', 'Dashboard')
@section('description')
@endsection
@section('content')
<main>
   <div class="container">
   <div class="row">
   @include('layouts.partials.sidebar') 
   <div class="col">
   <div class="page-title-container mb-3">
      <div class="row">
         <div class="col mb-2">
            <h1 class="mb-2 pb-0 display-4" id="title">Select Payment Method You want to use</h1>
            <div class="text-muted font-heading text-small">Deposit</div>
         </div>
      </div>
   </div>
   <h2 class="small-title"></h2>
   <div class="row mb-n5">
   <div class="col-xl-4 mb-5">
   <h2 class="small-title">Payment Method</h2>
   
         <div class="card mb-2">
            <a href="{{url('star2')}}" class="row g-0 sh-10">
               <div class="col-auto">
                  <div class="sw-9 sh-10 d-inline-block d-flex justify-content-center align-items-center">
                     <div class="fw-bold text-primary">
                        <div class="text-center">
                           <img src="/img/airtel.jpg" height="50" width="50" class="theme-filter" alt="access">
                        </div>
                     </div>
                  </div>
                  <div class="col">
                     <div class="card-body d-flex flex-column ps-0 pt-0 pb-0 h-100 justify-content-center">
                        <div class="d-flex flex-column">
                           <div class="text-alternate">AIRTEL MONEY</div>
                           <div class="text-small text-muted">Instant Approval</div>
                        </div>
                     </div>
                  </div>
            </a>
            </div>
            <div class="card mb-2">
               <a href="{{url('star1')}}" class="row g-0 sh-10">
                  <div class="col-auto">
                     <div class="sw-9 sh-10 d-inline-block d-flex justify-content-center align-items-center">
                        <div class="fw-bold text-primary">
                           <img src="/img/mtn.jpg" height="50" width="50" class="theme-filter" alt="access">
                        </div>
                     </div>
                  </div>
                  <div class="col">
                     <div class="card-body d-flex flex-column ps-0 pt-0 pb-0 h-100 justify-content-center">
                        <div class="d-flex flex-column">
                           <div class="text-alternate">MTN</div>
                           <div class="text-small text-muted">Instant Approval.</div>
                        </div>
                     </div>
                  </div>
               </a>
            </div>
           
         </div>
  
</main>
@endsection