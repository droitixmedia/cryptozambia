@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
     <div class="mobile-navbar-wrapper">

            
            <!-- header begin -->
            <div class="header header-style-4" id="header">
                <div class="top">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-5 col-lg-3">
                                <div class="welcome-text">
                                    <p>Welcome to Tranquility</p>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-9 d-xl-flex d-lg-flex d-block align-items-center">
                                <div class="part-right">
                                    <ul>
                                        <li>
                                            <span class="simple-text">Sever Time : </span>
                                            <div class="server-time">
                                                <div class="single-time clock-time">
                                                    <span class="icon">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                    <span class="text">
                                                        <span id="hours"></span>
                                                        <span id="minutes"></span>
                                                        <span id="seconds"></span>
                                                    </span>
                                                </div>
                                                <div class="single-time">
                                                    <span class="icon">
                                                        <i class="fas fa-calendar-alt"></i>
                                                    </span>
                                                    <span class="text">
                                                        <span id="date"></span>
                                                        <span id="month"></span>
                                                        <span id="year"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user-panel">
                                               
                                                   
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="bottom">
                    <div class="container">
                        <div class="row justify-content-between">
                            <div class="col-xl-3 col-lg-2 d-xl-flex d-lg-flex d-block align-items-center">
                                <div class="row">
                                    <div class="col-4 d-xl-none d-lg-none d-block">
                                        <button class="navbar-toggler" type="button">
                                            <span class="dag"></span>
                                            <span class="dag2"></span>
                                            <span class="dag3"></span>
                                        </button>    
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-8 d-xl-block d-lg-block d-flex align-items-center justify-content-end">
                                        <div class="logo">
                                            <a href="index.html">
                                                <img src="assets/img/logo2.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="mainmenu">
                                    <div class="d-xl-none d-lg-none d-block">
                                        <div class="user-profile">
                                            
                                        </div>
                                    </div>
                                    <nav class="navbar navbar-expand-lg">              

                                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                            <ul class="navbar-nav ml-auto">
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{url('/')}}">Home</a>
                                                </li>
                                                
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{url('about')}}">About Tranquility<span class="sr-only">(current)</span></a>
                                                </li>

                                     
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{url('contact')}}">Contact Us</a>
                                                </li>
                                                @guest
                                                <li class="nav-item join-now-btn">
                                                    <a class="nav-link" href="{{url('login')}}">Login</a>
                                                </li>
                                                @else

                                                 <li class="nav-item join-now-btn">
                                                    <a class="nav-link" href="{{ route('listings.create', [$area]) }}">Invest Now</a>
                                                </li>
                                                @endguest
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header end -->
            <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila">
                <div class="container">
                    <div class="row">
                        
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- register begin -->
            <div class="register">
                <div class="container">
                    <div class="reg-body">
                         <form action="{{route('verify')}}" method="POST" autocomplete="off">
                            @csrf

                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">

                                    
                                    @if(session('error'))
                                        <div class="alert alert-danger">
                                            {{ session('error') }}
                                        </div>
                                    @endif

                                    <h4 class="sub-title">Verify Your Phone number</h4>

                                    <div class="form-group">

                                        <input class="form-control{{ $errors->has('verification_code') ? ' is-invalid' : '' }}" name="verification_code" type="text" value="{{ old('verification_code') }}" required autofocus  placeholder="Enter OTP Code Sent To You">

                                        @error('verification_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <button class="def-btn btn-form" type="submit">Verify Phone Number</button>
                          
                                    </div>
                                   
                                
                            </div>
                            
                            <div class="row">
                                <div class="col-xl-6 col-lg-6">
                                    <div class="form-check">
                                        
                                    </div>
                                </div>
                               
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- register end --><br><br><br>

           
        </div> <!-- mobile navbar wrapper end -->

        <div class="d-xl-none d-lg-none d-block">
            <div class="mobile-navigation-bar">
                <ul>
                    <li>
                        <a href="#0">
                            <img src="assets/img/svg/profile.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#0">
                            <img src="assets/img/svg/money-transfering.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#0">
                            <img src="assets/img/svg/calculator.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#header">
                            <img src="assets/img/svg/arrow.svg" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="d-xl-block d-lg-block d-none">
            <div class="back-to-top-btn">
                <a href="#">
                    <img src="assets/img/svg/arrow.svg" alt="">
                </a>
            </div>
        </div>

@endsection
