<!DOCTYPE html>
<html lang="en" data-footer="true" data-override='{"attributes": {"placement": "horizontal", "layout": "boxed" }, "storagePrefix": "service-provider"}'>
<head>

   @include('layouts.partials.head')

</head>

<body>
      <div id="root">
       
 
  <div id="app">
   



   @include('layouts.partials.navigation')




       @yield('content')


      

     @include('layouts.partials.footer')

    
    </div>

   </div>
      <script src="/js/vendor/jquery-3.5.1.min.js"></script>
      <script src="/js/vendor/bootstrap.bundle.min.js"></script>
      <script src="/js/vendor/OverlayScrollbars.min.js"></script>
      <script src="/js/vendor/autoComplete.min.js"></script>
      <script src="/js/vendor/clamp.min.js"></script>
      <script src="/font/CS-Line/csicons.min.js"></script>
      <script src="/js/base/helpers.js"></script>
      <script src="/js/base/globals.js"></script>
      <script src="/js/base/nav.js"></script>
      <script src="/js/base/search.js"></script>
      <script src="/js/base/settings.js"></script>
      <script src="/js/base/init.js"></script>
      <script src="/js/common.js"></script>
      <script src="/js/scripts.js"></script>
      <!--Start of Tawk.to Script-->

<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/620b74881ffac05b1d79f027/1frubuef1';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
