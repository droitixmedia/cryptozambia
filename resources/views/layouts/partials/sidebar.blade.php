<div class="col-auto d-none d-lg-flex">
                     <ul class="sw-25 side-menu mb-0 primary" id="menuSide">
                        <li>
                           <a href="{{url('/dashboard')}}">
                           <i data-cs-icon="grid-1" class="icon" data-cs-size="18"></i>
                           <span class="label">Dashboard</span>
                           </a>
                           
                        </li>
                         
                         <li>
                           <a href="{{url('/dashboard')}}">
                           <i data-cs-icon="chart-4" class="icon" data-cs-size="18"></i>
                           <span class="label">Account Levels</span>
                           </a>
                           
                        </li>
                         <li>
                           <a href="{{url('referrals')}}">
                            <i data-cs-icon="shield" class="icon " data-cs-size="18"></i>
                           <span class="label">Downliners</span>
                           </a>
                           
                        </li>
                         <li>
                           <a href="{{url('/profile')}}">
                            <i data-cs-icon="user" class="icon " data-cs-size="18"></i>
                           <span class="label">Edit Profile</span>
                           </a>
                           
                        </li>
                        <li>
                           <a href="{{ route('listings.published.index', [$area]) }}">
                             <i data-cs-icon="router" class="icon" data-cs-size="18"></i>
                           <span class="label">Your Clicks</span>
                           </a>
                           
                        </li>
                        @guest
                        

                        @else
                         <li><a href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">  <i data-cs-icon="logout" class="icon" data-cs-size="18"></i><span class="label">Sign out</span></a>
                                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                                     </li>
                                                     @endguest

                                                       @if (session()->has('impersonate'))
                                                        <li>
                           <a href="{{ route('listings.create', [$area]) }}">
                             <i data-cs-icon="wifi" class="icon" data-cs-size="18"></i>
                           <span class="label">Deposit Admin</span>
                           </a>
                           
                        </li>

                                                       @endif
                           
                       @role('admin')
                          <li>
                           <a href="{{ route('listings.create', [$area]) }}">
                             <i data-cs-icon="wifi" class="icon" data-cs-size="18"></i>
                           <span class="label">Deposit Admin</span>
                           </a>
                           
                        </li>
                           
                        <li>
                           <a href="{{ url('/admin/impersonate') }}">
                             <i data-cs-icon="wifi" class="icon" data-cs-size="18"></i>
                           <span class="label">Impersonate</span>
                           </a>
                           
                        </li>
                        <li>
                           <a href="{{ url('/admin/users') }}">
                             <i data-cs-icon="wifi" class="icon" data-cs-size="18"></i>
                           <span class="label">Users</span>
                           </a>
                           
                        </li>
                         <li>
                           <a href="{{ url('/admin/paidusers') }}">
                             <i data-cs-icon="wifi" class="icon" data-cs-size="18"></i>
                           <span class="label">Updated</span>
                           </a>
                           
                        </li>
                        <li>
                           <a href="{{ url('/admin/investments') }}">
                             <i data-cs-icon="wifi" class="icon" data-cs-size="18"></i>
                           <span class="label">Investments</span>
                           </a>
                           
                        </li>
                        <li>
                           <a href="{{ url('/admin/withdrawals') }}">
                             <i data-cs-icon="wifi" class="icon" data-cs-size="18"></i>
                           <span class="label">Investments</span>
                           </a>
                           
                        </li>
                 
                       @endrole
                        
                     </ul>
                  </div>