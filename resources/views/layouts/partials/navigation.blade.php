
<div id="nav" class="nav-container d-flex">
            <div class="nav-content d-flex">
               <div class="logo position-relative">
                  <a href="{{url('/dashboard')}}">
                     <img src="/img/logo/dailytasks.png"   alt="logo">
                  </a>
               </div>
               <div class="user-container d-flex">
                  @guest
                   <a href="{{url('login')}}" class="btn btn-icon btn-icon-start btn-warning mt-3 ">
                                    <i data-cs-icon="user"></i>
                                    <span>Login</span>
                                    </a>&nbsp&nbsp
                   
                  <a href="{{url('register')}}" class="btn btn-icon btn-icon-start btn-success mt-3 ">
                                    <i data-cs-icon="plus"></i>
                                    <span>Create Account</span>
                                    </a>
                  
                  @else


                  <a href="#" class="d-flex user position-relative" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <img class="profile" alt="profile" src="/img/illustration/{{Auth::user()->accounttype}}.jpg">
                     <div class="name">{{Auth::user()->name}} {{Auth::user()->surname}}</div>
                          <button href="#" class="btn btn-icon btn-icon-start btn-success mt-3 ">

                   @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               
 

                         @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach




                          @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if(!$listing->type())
                            
                               
 

                         @php ($withdrawn += $listing->extamount)

                           @if ($loop->last)

                           @endif

                           @else

                       
                         @endif
                     @endforeach

                          <span>K{{$sum-$withdrawn}}</span>
                                    </button><br>
                                    @if (Auth::user()->accounttype==2)
                                    <button href="#" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    
                                    <span>Starter Account</span>

                                    </button>
                                    @elseif (Auth::user()->accounttype==4)
                                    <button href="#" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    
                                    <span> Kruger Rand Account</span>

                                    </button>
                                    @elseif (Auth::user()->accounttype==6)
                                    <button href="#" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    
                                    <span>USDC Account</span>

                                    </button>
                                    @elseif (Auth::user()->accounttype==8)
                                    <button href="#" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    
                                    <span> Litecoin Account</span>

                                    </button>
                                      @elseif (Auth::user()->accounttype==10)
                                    <button href="#" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    
                                    <span>Ethereum Account</span>

                                    </button>
                                      @elseif (Auth::user()->accounttype==12)
                                    <button href="#" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    
                                    <span>Bitcoin Account</span>

                                    </button>
                                      @elseif (Auth::user()->accounttype==14)
                                    <button href="#" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    
                                    <span>NFT Account</span>

                                    </button>
                                      @elseif (Auth::user()->accounttype==16)
                                    <button href="#" class="btn btn-icon btn-icon-start btn-danger mt-3 ">
                                    
                                    <span> Blockchain Account</span>

                                    </button>
                                   
                                    @endif           

                  </a>
                  @endguest
                  
               </div>
               
               <div class="menu-container flex-grow-1">
                  <ul id="menu" class="menu">
                    
                     
                  </ul>
               </div>
               <div class="mobile-buttons-container">
                  <a href="#" id="mobileMenuButton" class="menu-button">
                  <i data-cs-icon="menu"></i>
                  </a>
               </div>
            </div>
            <div class="nav-shadow"></div>
         </div>