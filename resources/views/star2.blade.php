 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">AIRTEL MONEY DEPOSIT</h1>
                              <div class="text-muted font-heading text-small">Deposit</div>
                           </div>
                        </div>
                     </div>
                     
           <div class="card mb-5">

<div class="card-body sh-50 d-flex align-items-center justify-content-center">
<div class="text-center">

<div class="cta-1">MOBILE MONEY: AIRTEL MONEY</div>

<div class="cta-3 text-primary mb-4">NAME ON ACCOUNT: MALUMO MALUMO</div>
<div class="cta-3 text-primary mb-4">NUMBER: 0777233186</div>




<div class="cta-3 text-primary mb-4">Whatsapp 0767969405 AFTER YOU DEPOSIT</div>

</div>
</div>
</div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection