@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container">
        <!-- Content Header (Page header) -->     
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="me-auto">
                    <h4 class="page-title">Upload Your Proof of Payment</h4>
                </div>
                
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                                     <form action="/uploaddocs" method="post" enctype="multipart/form-data">
{{ csrf_field() }}

                                <div class="form-group">

                                            <input type="hidden" name="name" value="0" class="form-control">

                                        </div>


<br />
<input type="file" class="form-control" name="cvs[]" multiple />
<br />
<input type="submit" class="btn  btn-success" value="Upload" />

</form> 

 </section>
        <!-- /.content -->    
      </div>
  </div>
  <!-- /.content-wrapper -->

@endsection
