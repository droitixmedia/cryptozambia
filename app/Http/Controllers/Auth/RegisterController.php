<?php

namespace openjobs\Http\Controllers\Auth;

use openjobs\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use openjobs\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => [ 'string', 'max:255'],
            'surname' => [ 'string', 'max:255'],
            'phone_number' => [ 'string', 'max:255'],
            'accounttype' => [ 'string', 'max:255'],
            'area_id' => [ 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \openjobs\User
     */
    protected function create(array $data)
    {
        $cookie = Cookie::get('referral');
        $referred_by = $cookie;

        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'phone_number' => $data['phone_number'],
            'area_id' => $data['area_id'],
            'accounttype' => $data['accounttype'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'referred_by' => $referred_by,
            'isVerified' => true,
        ]);

        // dispatch registered event
        event(new Registered($user));

        return $user;
    }
}
