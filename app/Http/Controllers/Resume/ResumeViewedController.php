<?php

namespace openjobs\Http\Controllers\Resume;

use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use Auth;

class ResumeViewedController extends Controller
{
    const INDEX_LIMIT = 10;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
            $user = Auth::user();

        $resumes = $request->user()
            ->viewedListings()->with(['area', 'user'])
            ->orderByPivot('updated_at', 'desc')
            ->isLive()
            ->take(self::INDEX_LIMIT)
            ->get();

        return view('user.resumes.viewed.index', [
            'resumes' => $resumes,
            'user'=>$user,
            'indexLimit' => self::INDEX_LIMIT,
        ]);
    }
}
