<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;
use openjobs\Category;

class StaticPagesController extends Controller

{

       public function withdraw(){
        return view('withdraw');
    }

       public function star1(){
        return view('star1');
    }

      public function star2(){
        return view('star2');
    }

      public function star3(){
        return view('star3');
    }

      public function star4(){
        return view('star4');
    }

      public function star5(){
        return view('star5');
    }

      public function star6(){
        return view('star6');
    }

      public function star7(){
        return view('star7');
    }

      public function star8(){
        return view('star8');
    }

     public function companyreg(){
        return view('auth.companyreg');
    }

      public function pay(Category $category){
        $categories = Category::withListings()->get()->toTree();
        return view('pay', compact('categories'));
    }

     public function paypal(){
        return view('paypal');
    }

     public function upgrade(){
        return view('upgrade');
    }

     public function bitcoin(){
        return view('bitcoin');
    }

     public function skrill(){
        return view('skrill');
    }

     public function bank(){
        return view('bank');
    }

      public function taken(){
        return view('taken');
    }
    public function about(){
        return view('pages.about');
    }

     public function cvwriting(){
        return view('pages.cvwriting');
    }
    public function bookacv(){
        return view('pages.bookacv');
    }
    public function terms(){
        return view('pages.terms');
    }

    public function privacy(){
        return view('pages.privacy');
    }

     public function faq(){
        return view('pages.faq');
    }

     public function test(){
        return view('pages.test');
    }




    public function contact(){
        return view('pages.contact');
    }



}
