<?php

namespace openjobs\Notifications;

use openjobs\Listing;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InvestmentMaturedNotification extends Notification
{
    use Queueable;

    private $investment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Listing $investment)
    {
        $this->investment = $investment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return array_merge($this->investment->toArray(), [
            'notification_message' => 'Great!!. Your investment has matured. View it here: ' . route('listings.published.index'),
        ]);
    }
}
